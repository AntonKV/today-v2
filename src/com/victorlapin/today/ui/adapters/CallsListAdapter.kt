package com.victorlapin.today.ui.adapters

import android.graphics.PorterDuff
import android.net.Uri
import android.provider.ContactsContract
import android.provider.MediaStore
import android.support.annotation.DrawableRes
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.inflate
import com.victorlapin.today.manager.ResourcesManager
import com.victorlapin.today.model.bean.CallsBean
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_calls_log.view.*
import android.util.TypedValue

class CallsListAdapter(
        private val mItems: List<CallsBean.LogItem>,
        private val mClickEvent: PublishSubject<Uri>,
        private val mResources: ResourcesManager
) : RecyclerView.Adapter<CallsListAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val mDurationString: String =
            mResources.getString(R.string.provider_calls_duration_template)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(parent.inflate(R.layout.item_calls_log))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val log = mItems[position]
        holder.itemView.lbl_name.text = log.name
        holder.itemView.lbl_date.text = mDurationString.format(log.date, log.duration)
        if (log.photoUri != null) {
            val bitmap = MediaStore.Images.Media.getBitmap(mResources.resolver, log.photoUri)
            val roundedBitmap = RoundedBitmapDrawableFactory.create(mResources.resources, bitmap)
            roundedBitmap.isCircular = true
            holder.itemView.image.setImageDrawable(roundedBitmap)
        } else {
            @DrawableRes val imageRes = when (log.numberType) {
                ContactsContract.CommonDataKinds.Phone.TYPE_MAIN,
                ContactsContract.CommonDataKinds.Phone.TYPE_COMPANY_MAIN,
                ContactsContract.CommonDataKinds.Phone.TYPE_WORK,
                ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE ->
                    R.drawable.domain
                ContactsContract.CommonDataKinds.Phone.TYPE_HOME ->
                    R.drawable.home
                else -> R.drawable.account_circle
            }

            val typedValue = TypedValue()
            val theme = holder.itemView.context.theme
            theme.resolveAttribute(android.R.attr.textColorTertiary, typedValue, true)
            val color = holder.itemView.context.getColor(typedValue.resourceId)

            val drawable = holder.itemView.context.getDrawable(imageRes)
            DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN)
            DrawableCompat.setTint(drawable, color)

            holder.itemView.image.setImageDrawable(drawable)
        }
        when (log.type) {
            CallsBean.CALL_TYPE_INCOMING -> {
                holder.itemView.img_call_type.setImageResource(R.drawable.call_received)
                holder.itemView.img_call_type.setColorFilter(
                        mResources.getColor(R.color.blue))
            }
            CallsBean.CALL_TYPE_OUTGOING -> {
                holder.itemView.img_call_type.setImageResource(R.drawable.call_made)
                holder.itemView.img_call_type.setColorFilter(
                        mResources.getColor(R.color.green))
            }
            CallsBean.CALL_TYPE_MISSED -> {
                holder.itemView.img_call_type.setImageResource(R.drawable.call_missed)
                holder.itemView.img_call_type.setColorFilter(
                        mResources.getColor(R.color.red))
            }
            else -> holder.itemView.img_call_type.visibility = View.GONE
        }

        if (log.contactUri != null) {
            holder.itemView.container.setOnClickListener {
                mClickEvent.onNext(log.contactUri!!)
            }
        } else {
            holder.itemView.container.setOnClickListener(null)
        }
    }

    override fun getItemCount(): Int = mItems.size
}