package com.victorlapin.today.ui.adapters.delegates

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.inflate
import com.victorlapin.today.model.BeanClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.OAuthRequestBean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.include_item_header.view.*
import kotlinx.android.synthetic.main.item_permissions.view.*

class OAuthDelegateAdapter(
        private val mClickEvent: PublishSubject<BeanClickEventArgs>
) : BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = OAuthViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as OAuthViewHolder).bind(item as OAuthRequestBean)
    }

    inner class OAuthViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_permissions)) {

        init {
            itemView.btn_customize.visibility = View.GONE
        }

        fun bind(bean: OAuthRequestBean) {
            itemView.lbl_header.text = bean.cardHeader
            itemView.lbl_description.text = itemView.context.getString(R.string.request_oauth)
            itemView.container.setOnClickListener {
                mClickEvent.onNext(BeanClickEventArgs(bean.beanType, null))
            }
        }
    }
}