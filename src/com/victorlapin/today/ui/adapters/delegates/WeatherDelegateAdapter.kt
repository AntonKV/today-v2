package com.victorlapin.today.ui.adapters.delegates

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.Screens
import com.victorlapin.today.inflate
import com.victorlapin.today.manager.SettingsManager
import com.victorlapin.today.model.EventType
import com.victorlapin.today.model.MenuClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.WeatherBean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import com.victorlapin.today.ui.adapters.WeatherForecastListAdapter
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.include_item_footer.view.*
import kotlinx.android.synthetic.main.include_item_header.view.*
import kotlinx.android.synthetic.main.item_weather.view.*

class WeatherDelegateAdapter(
        private val mSettings: SettingsManager,
        private val mMenuEvent: PublishSubject<MenuClickEventArgs>
) : BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = WeatherViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as WeatherViewHolder).bind(item as WeatherBean)
    }

    inner class WeatherViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_weather)) {
        private val humidityString =
                itemView.context.getString(R.string.provider_weather_humidity)
        private val tempLikeString =
                itemView.context.getString(R.string.provider_weather_temp_like)
        private val sourceString =
                itemView.context.getString(R.string.provider_source_template)
        private val titleString =
                itemView.context.getString(R.string.provider_weather_title_template)

        init {
            itemView.btn_customize.setOnClickListener { showPopup() }
        }

        fun bind(bean: WeatherBean) {
            itemView.lbl_header.text = bean.cardHeader
            itemView.lbl_description.text = bean.weather

            if (bean.title != null) {
                itemView.lbl_title.visibility = View.VISIBLE
                itemView.lbl_title.text = titleString.format(bean.title, bean.temp?.let {
                    (if (it > 1) "+" else "") + "%.0f".format(it) + "°" + bean.tempType
                })
            } else {
                itemView.lbl_title.visibility = View.GONE
            }

            if (bean.tempLike != null) {
                itemView.lbl_temp_like.visibility = View.VISIBLE
                itemView.lbl_temp_like.text = tempLikeString.format(bean.tempLike?.let {
                    (if (it > 1) "+" else "") + "%.0f".format(it) + "°" + bean.tempType
                })
            } else {
                itemView.lbl_temp_like.visibility = View.GONE
            }

            if (bean.humidity != null) {
                itemView.lbl_humidity.visibility = View.VISIBLE
                itemView.lbl_humidity.text = humidityString.format(bean.humidity, "%")
            } else {
                itemView.lbl_humidity.visibility = View.GONE
            }

            itemView.lbl_source.text = sourceString.format(bean.source, bean.lastUpdateTime)
            if (bean.image != null) {
                itemView.image.setImageDrawable(bean.image)
                itemView.image.visibility = View.VISIBLE
            } else {
                itemView.image.visibility = View.GONE
            }

            if (mSettings.isWeatherForecastEnabled) {
                itemView.list_forecast.visibility = View.VISIBLE
                itemView.list_forecast.layoutManager = GridLayoutManager(itemView.context, 3)
                itemView.list_forecast.setHasFixedSize(true)
                itemView.list_forecast.adapter = WeatherForecastListAdapter(bean.items)
            } else {
                itemView.list_forecast.visibility = View.GONE
            }
        }

        private fun showPopup() {
            val popupMenu = PopupMenu(itemView.context,
                    itemView.anchor)
            popupMenu.inflate(R.menu.item_context_full)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_context_customize -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.NAVIGATE,
                                Screens.ACTIVITY_SETTINGS,
                                Screens.FRAGMENT_SETTINGS_WEATHER))
                        true
                    }
                    R.id.action_context_disable -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.DISABLE_PROVIDER,
                                null,
                                "weather"))
                        true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
    }
}