package com.victorlapin.today.ui.adapters.delegates

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.inflate
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import com.victorlapin.today.util.Dimensions

class FooterDelegateAdapter: BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = FooterViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as FooterViewHolder).bind()
    }

    inner class FooterViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_footer)) {

        fun bind() {
            itemView.minimumHeight = if (Dimensions.hasSoftwareKeys(itemView.context))
                Dimensions.navigationBarHeight else 0
        }
    }
}