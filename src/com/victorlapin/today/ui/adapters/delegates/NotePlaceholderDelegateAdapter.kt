package com.victorlapin.today.ui.adapters.delegates

import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.inflate
import com.victorlapin.today.model.BeanClickEventArgs
import com.victorlapin.today.model.EventType
import com.victorlapin.today.model.MenuClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.NotePlaceholderBean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.include_item_header.view.*
import kotlinx.android.synthetic.main.item_placeholder.view.*

class NotePlaceholderDelegateAdapter(
        private val mClickEvent: PublishSubject<BeanClickEventArgs>,
        private val mMenuEvent: PublishSubject<MenuClickEventArgs>
) : BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = NotePlaceholderViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as NotePlaceholderViewHolder).bind(item as NotePlaceholderBean)
    }

    inner class NotePlaceholderViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_placeholder)) {

        init {
            itemView.btn_customize.setOnClickListener { showPopup() }
        }

        fun bind(bean: NotePlaceholderBean) {
            itemView.lbl_header.text = bean.cardHeader
            itemView.lbl_text.text = bean.text

            itemView.container.setOnClickListener {
                mClickEvent.onNext(BeanClickEventArgs(bean.beanType, null))
            }
        }

        private fun showPopup() {
            val popupMenu = PopupMenu(itemView.context,
                    itemView.anchor)
            popupMenu.inflate(R.menu.item_context_disable_only)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_context_disable -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.DISABLE_PROVIDER,
                                null,
                                "notes"))
                        true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
    }
}