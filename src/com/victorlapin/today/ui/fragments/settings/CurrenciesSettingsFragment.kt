package com.victorlapin.today.ui.fragments.settings

import android.os.Bundle
import android.support.v14.preference.MultiSelectListPreference
import android.support.v7.preference.ListPreference
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat
import com.victorlapin.today.R
import com.victorlapin.today.manager.SettingsManager
import com.victorlapin.today.model.database.dao.CurrenciesDao
import org.koin.android.ext.android.inject

class CurrenciesSettingsFragment : PreferenceFragmentCompat() {
    private val mSettings by inject<SettingsManager>()
    private val mCurrenciesDao by inject<CurrenciesDao>()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences_currencies)

        val symbolPreference =
                findPreference(SettingsManager.KEY_CURRENCIES_SYMBOL) as ListPreference
        val value = mSettings.currenciesSymbol
        symbolPreference.value = value
        symbolPreference.summary = value
        symbolPreference.onPreferenceChangeListener =
                Preference.OnPreferenceChangeListener { preference, newValue ->
                    preference.summary = newValue.toString()
                    mCurrenciesDao.clear()
                    true
                }

        val basesPreference =
                findPreference(SettingsManager.KEY_CURRENCIES_BASES) as MultiSelectListPreference
        val values = mSettings.currenciesBases
        basesPreference.values = values
        basesPreference.summary = mSettings.flatten(values)
        basesPreference.onPreferenceChangeListener =
                Preference.OnPreferenceChangeListener { preference, newValue ->
                    preference.summary = mSettings.flatten(newValue.toString())
                    mCurrenciesDao.clear()
                    true
                }
    }
}