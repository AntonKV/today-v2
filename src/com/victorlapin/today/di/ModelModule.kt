package com.victorlapin.today.di

import android.arch.persistence.room.Room
import com.victorlapin.today.model.database.AppDatabase
import com.victorlapin.today.model.interactor.*
import com.victorlapin.today.model.repository.*
import org.koin.dsl.module.module

val modelModule = module {
    factory { BeansInteractor(get(), get(), get(), get(), get(), get(), get(), get(), get()) }
    factory { AboutInteractor(get()) }
    factory { NotesInteractor(get()) }
    factory { AboutRepository(get(), get()) }
    factory { CalendarBeansRepository(get(), get(), get()) }
    factory { CallsBeansRepository(get(), get(), get()) }
    factory { CryptoCurrenciesBeansRepository(get(), get(), get(), get(), get()) }
    factory { CurrenciesBeansRepository(get(), get(), get(), get(), get()) }
    factory { FitnessBeansRepository(get(), get(), get()) }
    factory { MetaBeansRepository(get(), get(), get()) }
    factory { WearablesBeansRepository(get(), get()) }
    factory { WeatherBeansRepository(get(), get(), get(), get(), get(), get()) }
    factory { NotesBeansRepository(get(), get(), get()) }

    single {
        Room.databaseBuilder(get(), AppDatabase::class.java, "today.db")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
    }
    single { get<AppDatabase>().getCurrenciesDao() }
    single { get<AppDatabase>().getCryptoCurrenciesDao() }
    single { get<AppDatabase>().getWeatherDao() }
    single { get<AppDatabase>().getNotesDao() }
}