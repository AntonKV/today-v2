package com.victorlapin.today.sync

import android.util.Log
import androidx.work.*
import com.victorlapin.today.manager.SettingsManager
import com.victorlapin.today.model.DateBuilder
import com.victorlapin.today.model.interactor.BeansInteractor
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import java.util.concurrent.TimeUnit

class DataFetchWorker : Worker(), KoinComponent {
    private val mSettings by inject<SettingsManager>()
    private val mInteractor by inject<BeansInteractor>()

    override fun doWork(): Result {
        Log.d(DataFetchWorker::class.java.simpleName, "Sync started")
        mSettings.lastSync = System.currentTimeMillis()
        val dateBuilder = DateBuilder()
        return try {
            mInteractor.loadBeans(dateBuilder).blockingSubscribe()
            Log.d(DataFetchWorker::class.java.simpleName, "Sync complete")
            Result.SUCCESS
        } catch (t: Throwable) {
            Log.w(DataFetchWorker::class.java.simpleName,
                    "Sync failed: {$t.message}")
            Result.RETRY
        }
    }

    companion object {
        const val JOB_TAG = "DataFetchWorker"

        @JvmStatic
        fun buildRequest(): WorkRequest {
            val constraints = Constraints.Builder()
                    .setRequiresBatteryNotLow(true)
                    .setRequiredNetworkType(NetworkType.NOT_ROAMING)
                    .setRequiresCharging(false)
                    .setRequiresDeviceIdle(false)
                    .build()

            return PeriodicWorkRequest
                    .Builder(DataFetchWorker::class.java, 1, TimeUnit.HOURS)
                    .addTag(JOB_TAG)
                    .setConstraints(constraints)
                    .build()
        }
    }
}