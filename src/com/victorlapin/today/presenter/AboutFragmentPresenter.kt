package com.victorlapin.today.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.victorlapin.today.model.AboutClickEventArgs
import com.victorlapin.today.model.interactor.AboutInteractor
import com.victorlapin.today.view.AboutFragmentView
import ru.terrakok.cicerone.Router

@InjectViewState
class AboutFragmentPresenter(
        private val mInteractor: AboutInteractor,
        private val mRouter: Router
) : MvpPresenter<AboutFragmentView>() {
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        showData()
    }

    private fun showData() {
        val data = mInteractor.getData()
        viewState.setData(data)
    }

    fun onItemClick(args: AboutClickEventArgs) =
        mRouter.navigateTo(args.screenKey, args.data)
}