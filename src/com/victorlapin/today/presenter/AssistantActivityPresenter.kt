package com.victorlapin.today.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.victorlapin.today.Screens
import com.victorlapin.today.view.AssistantActivityView
import ru.terrakok.cicerone.Router

@InjectViewState
class AssistantActivityPresenter(
        private val mRouter: Router
) : MvpPresenter<AssistantActivityView>() {
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        mRouter.replaceScreen(Screens.FRAGMENT_TODAY)
    }
}