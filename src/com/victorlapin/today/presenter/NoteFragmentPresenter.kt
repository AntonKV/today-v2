package com.victorlapin.today.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.victorlapin.today.Screens
import com.victorlapin.today.model.database.entity.Note
import com.victorlapin.today.model.interactor.NotesInteractor
import com.victorlapin.today.view.NoteFragmentView
import ru.terrakok.cicerone.Router

@InjectViewState
class NoteFragmentPresenter(
        private val mInteractor: NotesInteractor,
        private val mRouter: Router
) : MvpPresenter<NoteFragmentView>() {
    fun showData(noteId: Long?) {
        if (noteId != null && noteId > -1) {
            val note = mInteractor.getNoteById(noteId)
            viewState.setData(note)
        }
    }

    fun onSaveClicked(note: Note) {
        if (note.id != null) {
            mInteractor.updateNote(note)
        } else {
            mInteractor.addNote(note)
        }
        mRouter.exitWithResult(Screens.EXIT_CODE_NOTE, 0)
    }
}