package com.victorlapin.today.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.victorlapin.today.Screens
import com.victorlapin.today.view.NoteActivityView
import ru.terrakok.cicerone.Router

@InjectViewState
class NoteActivityPresenter(
        private val mRouter: Router
) : MvpPresenter<NoteActivityView>() {
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        showFragment()
    }

    private fun showFragment() = mRouter.replaceScreen(Screens.FRAGMENT_NOTE)

    fun onBackPressed() = mRouter.exitWithResult(Screens.EXIT_CODE_NOTE, null)
}