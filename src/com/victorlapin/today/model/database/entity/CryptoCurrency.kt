package com.victorlapin.today.model.database.entity

import android.arch.persistence.room.*

@Entity(tableName = "crypto_currencies",
        foreignKeys = [(ForeignKey(entity = CryptoCurrencyMeta::class,
                parentColumns = ["id"], childColumns = ["meta_id"],
                onDelete = ForeignKey.CASCADE))],
        indices = [Index(name = "idx_crypto", value = ["meta_id"])])
data class CryptoCurrency(
        @PrimaryKey(autoGenerate = true)
        val id: Long? = null,
        val key: String,
        val data: String,
        @ColumnInfo(name = "meta_id")
        val metaId: Long
)

@Entity(tableName = "crypto_currencies_meta")
data class CryptoCurrencyMeta(
        @PrimaryKey(autoGenerate = true)
        val id: Long? = null,
        @ColumnInfo(name = "last_update_dt")
        val lastUpdateDT: Long
)

class CryptoCurrenciesWithMeta {
    @Embedded
    var meta: CryptoCurrencyMeta? = null

    @Relation(parentColumn = "id", entity = CryptoCurrency::class, entityColumn = "meta_id")
    var currencies: List<CryptoCurrency> = listOf()
}