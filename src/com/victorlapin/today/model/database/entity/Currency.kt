package com.victorlapin.today.model.database.entity

import android.arch.persistence.room.*

@Entity(tableName = "currencies",
        foreignKeys = [(ForeignKey(entity = CurrencyMeta::class,
                parentColumns = ["id"], childColumns = ["meta_id"],
                onDelete = ForeignKey.CASCADE))],
        indices = [Index(name = "idx", value = ["meta_id"])])
data class Currency(
        @PrimaryKey(autoGenerate = true)
        val id: Long? = null,
        val key: String,
        val data: String,
        @ColumnInfo(name = "meta_id")
        val metaId: Long
)

@Entity(tableName = "currencies_meta")
data class CurrencyMeta(
        @PrimaryKey(autoGenerate = true)
        val id: Long? = null,
        @ColumnInfo(name = "last_update_dt")
        val lastUpdateDT: Long
)

class CurrenciesWithMeta {
    @Embedded
    var meta: CurrencyMeta? = null

    @Relation(parentColumn = "id", entity = Currency::class, entityColumn = "meta_id")
    var currencies: List<Currency> = listOf()
}