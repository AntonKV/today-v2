package com.victorlapin.today.model.database.dao

import android.arch.persistence.room.*
import com.victorlapin.today.model.database.entity.CryptoCurrenciesWithMeta
import com.victorlapin.today.model.database.entity.CryptoCurrency
import com.victorlapin.today.model.database.entity.CryptoCurrencyMeta

@Dao
abstract class CryptoCurrenciesDao {
    @Transaction
    @Query("select * from crypto_currencies_meta limit 1")
    abstract fun getCryptoCurrencies(): CryptoCurrenciesWithMeta?

    @Transaction
    open fun insert(data: CryptoCurrenciesWithMeta) {
        data.meta?.let { clear(); insert(it) }
        data.currencies.forEach { insert(it) }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(data: CryptoCurrency)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(data: CryptoCurrencyMeta)

    @Query("delete from crypto_currencies_meta")
    abstract fun clear()
}