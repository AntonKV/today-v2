package com.victorlapin.today.model.database.dao

import android.arch.persistence.room.*
import com.victorlapin.today.model.database.entity.TodayWeather
import com.victorlapin.today.model.database.entity.WeatherForecast
import com.victorlapin.today.model.database.entity.WeatherWithForecast

@Dao
abstract class WeatherDao {
    @Transaction
    @Query("select * from weather limit 1")
    abstract fun getWeather(): WeatherWithForecast?

    @Transaction
    open fun insert(data: WeatherWithForecast) {
        data.weather?.let { clear(); insert(it) }
        data.forecast.forEach { insert(it) }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(data: WeatherForecast)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(data: TodayWeather)

    @Query("delete from weather")
    abstract fun clear()
}