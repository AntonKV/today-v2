package com.victorlapin.today.model.repository

import android.Manifest
import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import com.google.android.gms.awareness.state.Weather
import com.google.android.gms.tasks.Tasks
import com.victorlapin.today.R
import com.victorlapin.today.manager.*
import com.victorlapin.today.model.DateBuilder
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.WeatherBean
import com.victorlapin.today.model.database.dao.WeatherDao
import com.victorlapin.today.model.database.entity.TodayWeather
import com.victorlapin.today.model.database.entity.WeatherWithForecast
import com.victorlapin.today.model.database.entity.WeatherForecast
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import kotlin.collections.ArrayList

class WeatherBeansRepository(
        private val mSettings: SettingsManager,
        private val mHttpRepo: HttpManager,
        private val mResources: ResourcesManager,
        private val mServices: ServicesManager,
        private val mAccounts: AccountManager,
        private val mWeatherDao: WeatherDao
) : BeansRepository {
    interface MetaWeatherApi {
        @GET("location/search")
        fun getCity(@Query("lattlong") lattlong: String): Call<String>

        @GET("location/{id}")
        fun getWeather(@Path("id") id: Int): Call<String>
    }

    private val mMetaWeatherUrl = "https://www.metaweather.com/api/"
    private val mDelta: Long = TimeUnit.HOURS.toMillis(1)
    private var mLastWeather: WeatherWithForecast? = null
    private val mDefaultLocationString =
            mResources.getString(R.string.provider_weather_default_location)

    private fun hasPermission() =
            mAccounts.hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)

    override val name = "weather"

    override fun provideBeans(dateBuilder: DateBuilder): Observable<Bean> {
        if (mSettings.isProviderEnabled(name)) {
            mLastWeather = mWeatherDao.getWeather()
            if (isCacheValid(dateBuilder)) {
                return getCachedData()
                        .map { createBean(it) }
            }
            if (mServices.isConnectedToInternet()) {
                return if (hasPermission()) {
                    loadData(dateBuilder)
                            .map { createBean(it) }
                            .subscribeOn(Schedulers.io())
                } else {
                    Observable.just(
                            WeatherBean(name,
                                    mResources.getString(R.string.provider_weather))
                                    .toPermissionsRequestBean())
                }
            }
        }
        return Observable.empty()
    }

    override fun isCacheValid(dateBuilder: DateBuilder): Boolean {
        return mLastWeather != null && mLastWeather?.weather!!.lastUpdateDT > 0
                && dateBuilder.now - mLastWeather?.weather!!.lastUpdateDT <= mDelta
    }

    @SuppressLint("MissingPermission")
    private fun loadData(dateBuilder: DateBuilder): Observable<WeatherWithForecast> {
        return Observable.create { emitter ->
            var weatherInfo: TodayWeather? = null
            val forecastInfo = ArrayList<WeatherForecast>()

            var city = ""
            var cityId: Int? = null
            var location: Pair<Double, Double>? = null
            var errorFlag = true

            val api = mHttpRepo.getRetrofit(mMetaWeatherUrl).create(MetaWeatherApi::class.java)
            val client = mAccounts.getAwarenessSnapshotClient()
            client?.let {
                val task = it.location
                try {
                    val locResult = Tasks.await(task, mSettings.timeout.toLong(),
                            TimeUnit.SECONDS)
                    if (task.isSuccessful) {
                        location = Pair(locResult.location.latitude,
                                locResult.location.longitude)
                        errorFlag = false
                    } else {
                    }
                } catch (ignore: TimeoutException) {
                    Log.d("Weather", "Awareness location timed out")
                } catch (ignore: ExecutionException) {
                    Log.d("Weather", "Play services error")
                }
            }

            if (errorFlag) {
                // play services failed, use the old-fashioned way
                val criteria = Criteria()
                criteria.accuracy = Criteria.ACCURACY_COARSE
                val locProviders = mServices.locationManager.getProviders(criteria, true)
                locProviders.forEach {
                    if (location == null) {
                        mServices.locationManager.requestSingleUpdate(it, object : LocationListener {
                            override fun onLocationChanged(p0: Location?) {}
                            override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {}
                            override fun onProviderEnabled(p0: String?) {}
                            override fun onProviderDisabled(p0: String?) {}
                        }, mServices.looper)
                        val loc = mServices.locationManager.getLastKnownLocation(it)
                        loc?.let {
                            location = Pair(it.latitude, it.longitude)
                        }
                    }
                }
            }

            location?.let {
                try {
                    val addresses = mServices.geocoder.getFromLocation(it.first, it.second, 1)
                    city = addresses[0].locality
                } catch (ignore: Exception) {
                }
                try {
                    val str = api.getCity("${it.first},${it.second}").execute().body()
                    val json = JSONArray(str)
                    cityId = json.getJSONObject(0).getInt("woeid")
                    if (city == "") city = json.getJSONObject(0).getString("title")
                } catch (ignore: Exception) {
                    ignore.printStackTrace()
                }
            }

            cityId?.let {
                try {
                    val inSdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                    val outSdf = SimpleDateFormat("MMM dd", Locale.getDefault())
                    val str = api.getWeather(it).execute().body()
                    val json = JSONObject(str).getJSONArray("consolidated_weather")

                    // use play services first
                    client?.let {
                        try {
                            val task = it.weather
                            val weatherResult = Tasks.await(task, mSettings.timeout.toLong(),
                                    TimeUnit.SECONDS)
                            if (task.isSuccessful) {
                                val weather = weatherResult.weather
                                val conditions = weather.conditions
                                weatherInfo = TodayWeather(
                                        id = 1,
                                        provider = PROVIDER_GOOGLE,
                                        city = city,
                                        tempC = weather.getTemperature(Weather.CELSIUS).toDouble(),
                                        tempF = weather.getTemperature(Weather.FAHRENHEIT).toDouble(),
                                        tempLikeC = weather.getFeelsLikeTemperature(Weather.CELSIUS).toDouble(),
                                        tempLikeF = weather.getFeelsLikeTemperature(Weather.FAHRENHEIT).toDouble(),
                                        humidity = weather.humidity,
                                        condition1 = conditions[0],
                                        condition2 = if (conditions.size > 1) conditions[1] else conditions[0],
                                        condition = null,
                                        lastUpdateDT = dateBuilder.now
                                )
                            } else {
                            }
                        } catch (ignore: TimeoutException) {
                            Log.d("Weather", "Awareness weather timed out")
                        } catch (ignore: ExecutionException) {
                            Log.d("Weather", "Play services error")
                        }
                    }
                    // old-fashioned way
                    if (weatherInfo == null) {
                        val weather = json.getJSONObject(0)
                        weatherInfo = TodayWeather(
                                id = 1,
                                provider = PROVIDER_METAWEATHER,
                                city = city,
                                tempC = weather.getDouble("the_temp"),
                                tempF = weather.getDouble("the_temp") * 1.8f + 32,
                                humidity = weather.getInt("humidity"),
                                condition = weather.getString("weather_state_abbr"),
                                tempLikeC = null,
                                tempLikeF = null,
                                condition1 = null,
                                condition2 = null,
                                lastUpdateDT = dateBuilder.now
                        )
                    }

                    // now collect 3-day forecast data
                    //TODO: get rid of hardcoded foreign key ids
                    try {
                        (1..3)
                                .map { json.getJSONObject(it) }
                                .map {
                                    WeatherForecast(
                                            weatherId = 1,
                                            tempC = it.getDouble("the_temp"),
                                            tempF = it.getDouble("the_temp") * 1.8f + 32,
                                            condition = it.getString("weather_state_abbr"),
                                            date = outSdf.format(inSdf.parse(it.getString("applicable_date")))
                                    )
                                }
                                .forEach { forecastInfo.add(it) }
                    } catch (ex: JSONException) {
                        ex.printStackTrace()
                        forecastInfo.clear()
                    }
                } catch (ignore: Exception) {
                    ignore.printStackTrace()
                }
            }

            weatherInfo?.let {
                val result = WeatherWithForecast()
                result.weather = it
                result.forecast = forecastInfo
                mLastWeather = result
                mWeatherDao.insert(result)
                emitter.onNext(result)
            }
            emitter.onComplete()
        }
    }

    private fun getCachedData(): Observable<WeatherWithForecast> =
            Observable.just(mLastWeather)

    private fun createBean(data: WeatherWithForecast): Bean {
        val unit = mSettings.weatherUnits
        val bean = WeatherBean(name, mResources.getString(R.string.provider_weather))
        data.weather!!.let {
            bean.source = it.provider
            bean.lastUpdateTime = DateBuilder.formatter.format(Date(it.lastUpdateDT))

            bean.title = it.city
            if (TextUtils.isEmpty(bean.title)) {
                bean.title = mDefaultLocationString
            }
            if (unit == "C") {
                bean.temp = it.tempC
                it.tempLikeC?.let {
                    bean.tempLike = it
                }
            } else if (unit == "F") {
                bean.temp = it.tempF
                it.tempLikeF?.let {
                    bean.tempLike = it
                }
            }
            bean.temp?.let {
                // avoiding -0 temperature
                if (it < 0 && it > -1) {
                    bean.temp = it * -1
                }
            }
            bean.tempLike?.let {
                // avoiding -0 temperature
                if (it < 0 && it > -1) {
                    bean.tempLike = it * -1
                }
            }
            bean.tempType = unit
            bean.humidity = it.humidity

            if (it.provider == PROVIDER_GOOGLE) {
                bean.weather = getWeatherString(it.condition1!!, it.condition2!!)
                bean.image = getWeatherDrawable(it.condition1, it.condition2)
            } else if (it.provider == PROVIDER_METAWEATHER) {
                bean.weather = getWeatherString(it.condition!!)
                bean.image = getWeatherDrawable(it.condition)
            }
        }

        if (mSettings.isWeatherForecastEnabled) {
            data.forecast.forEach {
                val item = WeatherBean.Forecast()
                if (unit == "C") {
                    item.temp = it.tempC
                } else if (unit == "F") {
                    item.temp = it.tempF
                }
                item.temp?.let {
                    // avoiding -0 temperature
                    if (it < 0 && it > -1) {
                        item.temp = it * -1
                    }
                }
                item.date = it.date
                item.image = getWeatherDrawable(it.condition)
                item.tempType = unit
                bean.items.add(item)
            }
        }
        return bean
    }

    private fun getWeatherString(condition: String): String {
        var stringId: Int = -1

        when (condition) {
            "sn" -> stringId = R.string.provider_weather_condition_snowy
            "sl" -> stringId = R.string.provider_weather_condition_rain_snow
            "h" -> stringId = R.string.provider_weather_condition_hail
            "t" -> stringId = R.string.provider_weather_condition_stormy
            "hr" -> stringId = R.string.provider_weather_condition_rainy
            "lr" -> stringId = R.string.provider_weather_condition_light_rain
            "s" -> stringId = R.string.provider_weather_condition_showers
            "hc" -> stringId = R.string.provider_weather_condition_cloudy
            "lc" -> stringId = R.string.provider_weather_condition_partly_sunny
            "c" -> stringId = R.string.provider_weather_condition_clear
        }

        return if (stringId != -1) mResources.getString(stringId) else
            mResources.getString(R.string.provider_weather_condition_unknown)
    }

    private fun getWeatherDrawable(condition: String): Drawable? {
        var drawableId: Int = -1

        when (condition) {
            "sn" -> drawableId = R.drawable.snow
            "sl" -> drawableId = R.drawable.icy
            "h" -> drawableId = R.drawable.icy
            "t" -> drawableId = R.drawable.storm
            "hr" -> drawableId = R.drawable.rain
            "lr" -> drawableId = R.drawable.light_rain
            "s" -> drawableId = R.drawable.rain
            "hc" -> drawableId = R.drawable.cloudy
            "lc" -> drawableId = R.drawable.partly_sunny
            "c" -> drawableId = R.drawable.clear
        }

        return if (drawableId != -1) mResources.getDrawable(drawableId) else null
    }

    private fun getWeatherString(c1: Int, c2: Int): String {
        var stringId = mConditionStringsMatrix[c1][c2]
        if (stringId == -1) {
            stringId = mConditionStringsMatrix[c1][c1]
        }

        return if (stringId != -1) mResources.getString(stringId) else
            mResources.getString(R.string.provider_weather_condition_unknown)
    }

    private fun getWeatherDrawable(c1: Int, c2: Int): Drawable? {
        var drawableId = mConditionDrawablesMatrix[c1][c2]
        if (drawableId == -1) {
            drawableId = mConditionDrawablesMatrix[c1][c1]
        }

        return if (drawableId != -1) mResources.getDrawable(drawableId) else null
    }

    companion object {
        private const val PROVIDER_GOOGLE = "Google Awareness"
        private const val PROVIDER_METAWEATHER = "MetaWeather.com"

        private val mConditionDrawablesMatrix = arrayOf(
                intArrayOf(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1), // unknown
                intArrayOf(-1, R.drawable.clear, R.drawable.partly_sunny, R.drawable.haze, R.drawable.haze, -1, R.drawable.light_rain, R.drawable.light_snow, -1, -1), // clear
                intArrayOf(-1, R.drawable.partly_cloudy, R.drawable.cloudy, -1, -1, -1, R.drawable.chance_of_rain, -1, -1, -1), // cloudy
                intArrayOf(-1, R.drawable.haze, -1, R.drawable.fog, -1, -1, -1, -1, -1, -1), // foggy
                intArrayOf(-1, -1, -1, -1, R.drawable.haze, -1, -1, -1, -1, -1), // hazy
                intArrayOf(-1, -1, -1, -1, -1, R.drawable.icy, -1, -1, -1, -1), // icy
                intArrayOf(-1, R.drawable.light_rain, -1, -1, -1, -1, R.drawable.rain, R.drawable.icy, -1, -1), // rainy
                intArrayOf(-1, R.drawable.light_snow, -1, -1, -1, -1, R.drawable.icy, R.drawable.snow, -1, -1), // snowy
                intArrayOf(-1, -1, -1, -1, -1, -1, -1, -1, R.drawable.storm, -1), // stormy
                intArrayOf(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1)  // windy
        )

        private val mConditionStringsMatrix = arrayOf(
                intArrayOf(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1), // unknown
                intArrayOf(-1, R.string.provider_weather_condition_clear, R.string.provider_weather_condition_partly_sunny, R.string.provider_weather_condition_hazy, R.string.provider_weather_condition_hazy, -1, R.string.provider_weather_condition_light_rain, R.string.provider_weather_condition_light_snow, -1, -1), // clear
                intArrayOf(-1, R.string.provider_weather_condition_partly_cloudy, R.string.provider_weather_condition_cloudy, -1, -1, -1, R.string.provider_weather_condition_chance_of_rain, -1, -1, -1), // cloudy
                intArrayOf(-1, R.string.provider_weather_condition_hazy, -1, R.string.provider_weather_condition_foggy, -1, -1, -1, -1, -1, -1), // foggy
                intArrayOf(-1, -1, -1, -1, R.string.provider_weather_condition_hazy, -1, -1, -1, -1, -1), // hazy
                intArrayOf(-1, -1, -1, -1, -1, R.string.provider_weather_condition_icy, -1, -1, -1, -1), // icy
                intArrayOf(-1, R.string.provider_weather_condition_light_rain, -1, -1, -1, -1, R.string.provider_weather_condition_rainy, R.string.provider_weather_condition_rain_snow, -1, -1), // rainy
                intArrayOf(-1, R.string.provider_weather_condition_light_snow, -1, -1, -1, -1, R.string.provider_weather_condition_rain_snow, R.string.provider_weather_condition_snowy, -1, -1), // snowy
                intArrayOf(-1, -1, -1, -1, -1, -1, -1, -1, R.string.provider_weather_condition_stormy, -1), // stormy
                intArrayOf(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1)  // windy
        )
    }
}