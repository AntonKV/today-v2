package com.victorlapin.today.model.repository

import com.victorlapin.today.R
import com.victorlapin.today.manager.HttpManager
import com.victorlapin.today.manager.ResourcesManager
import com.victorlapin.today.manager.ServicesManager
import com.victorlapin.today.manager.SettingsManager
import com.victorlapin.today.model.DateBuilder
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.CurrenciesBean
import com.victorlapin.today.model.database.dao.CurrenciesDao
import com.victorlapin.today.model.database.entity.CurrenciesWithMeta
import com.victorlapin.today.model.database.entity.Currency
import com.victorlapin.today.model.database.entity.CurrencyMeta
import io.reactivex.Observable
import org.json.JSONObject
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.Date
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class CurrenciesBeansRepository(
        private val mSettings: SettingsManager,
        private val mHttpRepo: HttpManager,
        private val mResources: ResourcesManager,
        private val mServices: ServicesManager,
        private val mCurrenciesDao: CurrenciesDao
) : BeansRepository {
    interface FixerApi {
        @GET("latest")
        fun getLatest(@Query("base") base: String,
                      @Query("symbols") symbol: String): Observable<String>
    }

    private val mFixerUrl = "http://api.fixer.io/"
    private val mDelta: Long = TimeUnit.HOURS.toMillis(3)
    private var mLastCurrencies: CurrenciesWithMeta? = null

    override val name = "currencies"

    override fun provideBeans(dateBuilder: DateBuilder): Observable<Bean> {
        if (mSettings.isProviderEnabled(name)) {
            mLastCurrencies = mCurrenciesDao.getCurrencies()
            if (isCacheValid(dateBuilder)) {
                return getCachedData()
                        .map { createBean(it) }
            }
            if (mServices.isConnectedToInternet()) {
                return loadData(dateBuilder)
                        .map { createBean(it) }
            }
        }
        return Observable.empty()
    }

    override fun isCacheValid(dateBuilder: DateBuilder): Boolean {
        return mLastCurrencies != null && mLastCurrencies?.meta!!.lastUpdateDT > 0
                && dateBuilder.now - mLastCurrencies?.meta!!.lastUpdateDT <= mDelta
    }

    private fun loadData(dateBuilder: DateBuilder): Observable<CurrenciesWithMeta> {
        val symbol = mSettings.currenciesSymbol
        val bases = mSettings.currenciesBases
        val observables = ArrayList<Observable<String>>()
        val api = mHttpRepo.getRetrofit(mFixerUrl).create(FixerApi::class.java)
        try {
            bases.forEach { observables.add(
                    api.getLatest(it, symbol)
                            .onErrorResumeNext { t: Throwable ->
                                t.printStackTrace()
                                Observable.empty()
                            })
            }

            return Observable.zip(observables) { results ->
                val curs = ArrayList<Currency>()
                results.forEach {
                    val json = JSONObject(it.toString())
                    val rates = json.getJSONObject("rates")

                    val base = json.getString("base")
                    if (rates.keys().hasNext()) {
                        val symbolTo = rates.keys().next()
                        val amount = rates.getDouble(symbol)
                        //TODO: get rid of hardcoded foreign key ids
                        curs.add(Currency(
                                metaId = 1,
                                key = base,
                                data = "1 $base = ${"%.2f".format(amount)} $symbolTo"))
                    }
                }

                val result = CurrenciesWithMeta()
                result.meta = CurrencyMeta(id = 1, lastUpdateDT = dateBuilder.now)
                result.currencies = curs
                mLastCurrencies = result
                mCurrenciesDao.insert(result)
                return@zip result
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return Observable.empty()
        }
    }

    private fun getCachedData(): Observable<CurrenciesWithMeta> =
            Observable.just(mLastCurrencies)

    private fun createBean(data: CurrenciesWithMeta): Bean {
        val bean = CurrenciesBean(name, mResources.getString(R.string.provider_currencies))
        bean.source = "Fixer.io"
        bean.lastUpdateTime = DateBuilder.formatter.format(Date(data.meta!!.lastUpdateDT))
        data.currencies.forEach {
            bean.items.add(it.data)
        }
        return bean
    }
}