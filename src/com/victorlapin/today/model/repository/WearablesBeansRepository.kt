package com.victorlapin.today.model.repository

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import com.victorlapin.today.R
import com.victorlapin.today.manager.ResourcesManager
import com.victorlapin.today.manager.SettingsManager
import com.victorlapin.today.model.DateBuilder
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.WearablesBean
import io.reactivex.Observable

class WearablesBeansRepository(
        private val mSettings: SettingsManager,
        private val mResources: ResourcesManager
) : BeansRepository {
    private val mAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()

    override val name = "wearables"

    override fun provideBeans(dateBuilder: DateBuilder): Observable<Bean> {
        if (mSettings.isProviderEnabled(name) && mAdapter != null) {
            val devices = mAdapter.bondedDevices
            if (devices.isNotEmpty()) {
                return Observable.create<Bean> { emitter ->
                    val bean = WearablesBean(name,
                            mResources.getString(R.string.provider_wearables))
                    bean.source = "Bluetooth"
                    devices
                            .filter { it.bondState == BluetoothDevice.BOND_BONDED }
                            .forEach { bean.devices.add(it.name) }
                    emitter.onNext(bean)
                    emitter.onComplete()
                }
            }
        }
        return Observable.empty()
    }

    override fun isCacheValid(dateBuilder: DateBuilder) = true
}