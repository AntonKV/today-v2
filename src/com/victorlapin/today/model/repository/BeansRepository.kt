package com.victorlapin.today.model.repository

import com.victorlapin.today.model.DateBuilder
import com.victorlapin.today.model.bean.Bean
import io.reactivex.Observable

interface BeansRepository {
    val name: String
    fun provideBeans(dateBuilder: DateBuilder): Observable<Bean>
    fun isCacheValid(dateBuilder: DateBuilder): Boolean
}