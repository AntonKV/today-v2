package com.victorlapin.today.model.bean

import android.Manifest

class StepsBean(provider: String, header: String) :
        Bean(provider, StepsBean.ID, header) {

    var steps = 0
    var lastUpdateTime: String? = null
    var distance = 0f
    var calories = 0f

    override val beanType = Bean.BEAN_TYPE_STEPS

    override fun getRequiredPermissions() =
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)

    companion object {
        const val ID = 250L
    }
}
