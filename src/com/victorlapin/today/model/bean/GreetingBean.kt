package com.victorlapin.today.model.bean

import java.text.SimpleDateFormat
import java.util.*

class GreetingBean(appName: String, val accountName: String?) :
        Bean("greeting", GreetingBean.ID, "greeting") {
    init {
        isDataBean = false
        excludeFromAssistant = true

        val sdf = SimpleDateFormat.getDateInstance()
        title = "$appName - ${sdf.format(Date())}"
    }

    override val beanType = Bean.BEAN_TYPE_GREETING

    companion object {
        const val ID = 2L
    }
}