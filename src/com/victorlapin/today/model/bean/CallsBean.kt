package com.victorlapin.today.model.bean

import android.Manifest
import android.net.Uri

class CallsBean(provider: String, header: String) :
        Bean(provider, CallsBean.ID, header) {

    class LogItem {
        var name: String? = null
        var type: String? = null
        var date: String? = null
        var duration: String? = null
        var photoUri: Uri? = null
        var numberType = 0
        var contactUri: Uri? = null
    }

    val items = ArrayList<LogItem>()

    override val beanType = Bean.BEAN_TYPE_CALLS

    override fun getRequiredPermissions() =
            arrayOf(Manifest.permission.READ_CALL_LOG, Manifest.permission.READ_CONTACTS)

    companion object {
        const val ID = 200L
        const val CALL_TYPE_INCOMING = "IN"
        const val CALL_TYPE_OUTGOING = "OUT"
        const val CALL_TYPE_MISSED = "MIS"
    }
}