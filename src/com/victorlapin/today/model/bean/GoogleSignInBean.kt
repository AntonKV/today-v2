package com.victorlapin.today.model.bean

class GoogleSignInBean : Bean("google", GoogleSignInBean.ID, "") {
    init {
        isDataBean = false
        isPolymorphBean = true
    }

    override val beanType = Bean.BEAN_TYPE_SIGN_IN_GOOGLE

    companion object {
        const val ID = 11L
    }
}