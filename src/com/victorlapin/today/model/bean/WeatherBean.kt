package com.victorlapin.today.model.bean

import android.Manifest
import android.graphics.drawable.Drawable

class WeatherBean(provider: String, header: String) :
        Bean(provider, WeatherBean.ID, header) {

    class Forecast {
        var date: String? = null
        var temp: Double? = null
        var image: Drawable? = null
        var tempType: String? = null
    }

    var weather: String? = null
    var temp: Double? = null
    var tempLike: Double? = null
    var tempType: String? = null
    var humidity: Int? = null
    var lastUpdateTime: String? = null
    var image: Drawable? = null
    val items = ArrayList<Forecast>()

    override val beanType = Bean.BEAN_TYPE_WEATHER

    override fun getRequiredPermissions() =
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)

    companion object {
        const val ID = 20L
    }
}