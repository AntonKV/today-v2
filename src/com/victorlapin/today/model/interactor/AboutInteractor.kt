package com.victorlapin.today.model.interactor

import com.victorlapin.today.model.repository.AboutRepository

class AboutInteractor(private val mRepo: AboutRepository) {
    fun getData() = mRepo.getData()
}