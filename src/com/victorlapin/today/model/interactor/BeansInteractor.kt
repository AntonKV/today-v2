package com.victorlapin.today.model.interactor

import com.victorlapin.today.model.DateBuilder
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.repository.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

class BeansInteractor(
        private val mMetaRepo: MetaBeansRepository,
        private val mCurrenciesRepo: CurrenciesBeansRepository,
        private val mWearablesRepo: WearablesBeansRepository,
        private val mCalendarRepo: CalendarBeansRepository,
        private val mCallsRepo: CallsBeansRepository,
        private val mFitnessRepo: FitnessBeansRepository,
        private val mWeatherRepo: WeatherBeansRepository,
        private val mCryptoCurrenciesRepo: CryptoCurrenciesBeansRepository,
        private val mNotesRepo: NotesBeansRepository
) {
    fun loadBeans(db: DateBuilder): Observable<Bean> {
        return mMetaRepo.provideBeans(db)
                .mergeWith(mCurrenciesRepo.provideBeans(db))
                .mergeWith(mWearablesRepo.provideBeans(db))
                .mergeWith(mCalendarRepo.provideBeans(db))
                .mergeWith(mCallsRepo.provideBeans(db))
                .mergeWith(mFitnessRepo.provideBeans(db))
                .mergeWith(mWeatherRepo.provideBeans(db))
                .mergeWith(mCryptoCurrenciesRepo.provideBeans(db))
                .mergeWith(mNotesRepo.provideBeans(db))
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun loadWeather(db: DateBuilder): Observable<Bean> {
        return mWeatherRepo.provideBeans(db)
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun loadCurrencies(db: DateBuilder): Observable<Bean> {
        return mCurrenciesRepo.provideBeans(db)
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun loadCalls(db: DateBuilder): Observable<Bean> {
        return mCallsRepo.provideBeans(db)
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun loadMeta(db: DateBuilder): Observable<Bean> {
        return mMetaRepo.provideBeans(db)
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun loadCryptoCurrencies(db: DateBuilder): Observable<Bean> {
        return mCryptoCurrenciesRepo.provideBeans(db)
                .observeOn(AndroidSchedulers.mainThread())
    }
}